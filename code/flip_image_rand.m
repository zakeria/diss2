function flipped_patch = flip_image_rand(patch) 
flip = 1; % The probability of flip is the on case.
flip_result = randi([0,1], 1);

if(flip_result == flip)
    flipped_patch = flipud(patch);
else
    flipped_patch = patch;
end
end
