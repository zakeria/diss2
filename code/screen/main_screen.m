% Main_screen contains the application logic.
% No oop-style fields, so set fields with guidata()
function [varargout]= main_screen(varargin)
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_ui_OpeningFcn, ...
                   'gui_OutputFcn',  @main_ui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end

% Initialise the main screen.
function main_ui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main_ui (see VARARGIN)
 
% Choose default command line output for main_ui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles); 

% UIWAIT makes main_ui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Define the variables
handles.application_configs = varargin{1};
handles.image_store = varargin{2};

handles.patch_count = 1; % Number of patches extracted from the current image
handles.current_patch_idx = 1; % Initial patch size
handles.current_image = zeros(1,1);
% Data matrix for this session.
%handles.data_matrix = zeros(1, 1); % dynamically changes

% Now a cell instead
% Containing: patch size at {x}{y}: struct{response, file_info};
% Where x is row of current trial, y is current patch size along that trial.
handles.data_matrix = {}; 
handles.row_idx = 1; % also the current number of trials.(i.e. full patch range).

set(handles.text_num_trials, 'string', "Number of trials: " + num2str(0));
    
guidata(hObject, handles); % Update handles.

continue_application(hObject, handles);
end

% Progress the application by increasing the patch size, saving etc.
function continue_application(hObject, handles) 
% hObject    handle to continue_fun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

application_configs = handles.application_configs;

patches_per_image = application_configs.patches_per_image;
patch_range_count = application_configs.patch_range_count;
patch_count = handles.patch_count;
patch_index = handles.current_patch_idx;

% Initial setup for the left/right panels.
if(patch_count == 1 && patch_index == 1)
    % save data
    % increment vars
    % update guidata    
    
    [normal_idx, hObject_local, handles_local, file_info] = update_gui_state(hObject, handles);%
    [response_, save_status] = get_input(normal_idx, application_configs, handles_local.data_matrix, patch_index, 0);
    
    spam_save_block = save_status;
    
    [updated_matrix, updated_row_idx, return_code] = update_data_matrix(handles_local, patch_index, save_status, handles_local.data_matrix);
    if(return_code ==  1)
        return;
    end
    
    handles_local.data_matrix = updated_matrix;
    handles_local.row_idx = updated_row_idx;
    
    while(response_~= 0 && response_~=1)
        [response_, save_status] = get_input(normal_idx, application_configs, handles_local.data_matrix, patch_index, spam_save_block);
        [updated_matrix, updated_row_idx, return_code] = update_data_matrix(handles_local, patch_index, save_status, handles_local.data_matrix);
        if(return_code ==  1)
            return;
        end
        handles_local.row_idx = updated_row_idx;
        handles_local.data_matrix = updated_matrix;
        java.lang.Thread.sleep(1);
    end
    response_struct = struct;
    response_struct.response = response_;
    response_struct.file_info = file_info;    
    handles_local.data_matrix{handles.row_idx, patch_index} = response_struct;
    
    guidata(hObject_local, handles_local);
    continue_application(hObject_local, handles_local);
    return;
end

% Remaining cases 
%---
% Reset once passed range.
% Increment data indices.
if(patch_count == patches_per_image && patch_index > patch_range_count)
    handles.patch_count = 1;
    handles.current_patch_idx = 1;
    
    data_row_idx = handles.row_idx;
       
    handles.row_idx = data_row_idx + 1;
    
    set(handles.text_num_trials, 'string', "Number of trials: " + num2str(handles.row_idx - 1)); % Not yet completed the next trial. 
        
    guidata(hObject, handles);
    continue_application(hObject, handles);
    return;
end

% increment for multiple patch selection mode only.
if(patch_index > patch_range_count && patch_count < patches_per_image) 
    handles.current_patch_idx = 1;
    handles.patch_count = patch_count + 1;    
    guidata(hObject, handles);
    continue_application(hObject, key_event, handles);
    % handle data save for this mode?
    return;
end

[normal_idx, hObject_local, handles_local, file_info] = update_gui_state(hObject, handles);
[response_, save_status] = get_input(normal_idx, application_configs, handles_local.data_matrix, patch_index, 0);

spam_save_block = save_status;

[updated_matrix, updated_row_idx, return_code] = update_data_matrix(handles_local, patch_index, save_status, handles_local.data_matrix);
if(return_code ==  1)
    return;
end
handles_local.data_matrix = updated_matrix;
handles_local.row_idx = updated_row_idx;

while(response_~= 0 && response_~=1)    
    [response_, save_status] = get_input(normal_idx, application_configs, handles_local.data_matrix, patch_index, spam_save_block);        
    [updated_matrix, updated_row_idx, return_code] = update_data_matrix(handles_local, patch_index, save_status, handles_local.data_matrix);
    if(return_code ==  1)
        return;
    end
    handles_local.row_idx = updated_row_idx;
    handles_local.data_matrix = updated_matrix;    
    java.lang.Thread.sleep(1);
end
response_struct = struct;
response_struct.response = response_;
response_struct.file_info = file_info;    
handles_local.data_matrix{handles.row_idx, patch_index} = response_struct;

guidata(hObject_local, handles_local);
continue_application(hObject_local, handles_local); 
end


% Update the data matrix after a successful save.
function [updated_matrix, updated_row_idx, return_code] = update_data_matrix(handles, patch_index, save_status, data_matrix)
configs = handles.application_configs;
patch_range = configs.patch_range_count;

updated_matrix = data_matrix;
row_idx = handles.row_idx;
updated_row_idx = row_idx;

return_code = 0; % continue.

if(save_status == 0)
    return;    
else   

% Notify and exit.
set(handles.status_text, 'string', "Thank you for your participation.")
pause(5);
return_code = 1; % application exit. 
close(handles.figure1);
end
end

% --- Outputs from this function are returned to the command line.
function [varargout]= main_ui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
%varargout{1} = handles.output;
end
