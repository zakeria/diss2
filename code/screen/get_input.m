function [response_, save_status] = get_input(normal_idx, configs, data_matrix, patch_index, spam_save_block)
save_status = 0; % general case.
while(true)
    w = waitforbuttonpress;
    key_press = get(gcf,'currentch');
    if (key_press == 'q')
        % save data, increment row,col idx for matrix: patch size incr=col
        % only need a current row variable.
        idx_ = 1;
        response_ = (idx_ == normal_idx);
        break;
    elseif (key_press == 'p')
        idx_ = 2;
        response_ = (idx_ == normal_idx);
        break;
    elseif (key_press == 's')
        save_status = save_responses(normal_idx, configs, data_matrix, patch_index, spam_save_block);
        response_ = 3; % Save data response.
        break;
    end
end
set(gcf,'currentch',char(1)); % Reset the current key for next read.
end