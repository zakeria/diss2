% Remember to Return  states to be used to update for local scope.
function [normal_idx, hObject, handles, file_info] = update_gui_state(hObject, handles)
patch_index = handles.current_patch_idx;

% Update this context.
[normal_idx, handles_updated, hobject_updated, file_info] = configure_patch_panels(hObject, handles);
hObject = hobject_updated;
handles = handles_updated;

handles.current_patch_idx = patch_index + 1;
guidata(hObject, handles);
end