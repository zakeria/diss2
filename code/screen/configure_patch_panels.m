% Configure the panels.
% For each image we traverse all patch sizes.
function [normal_idx, handles_updated, hobject_updated, file_info] = configure_patch_panels(hObject, handles)
left_panel = 1; % Left panel index is 1.

file_count = handles.application_configs.image_file_count;
random_idx = randi(file_count);

patch_range = handles.application_configs.patch_range;
patch_idx = handles.current_patch_idx;
patch_size = patch_range(patch_idx);

patch_count = handles.patch_count;
patch_index = handles.current_patch_idx;


multiple_patches_per_img = handles.application_configs.multiple_patches_per_img;

if(patch_count == 1 && patch_index == 1) % Initial.
    [random_image, file_info] = get_random_image(handles.image_store, random_idx, patch_size);    
else
    if(multiple_patches_per_img)
        [random_image, file_info] = handles.current_image;
    else
        [random_image, file_info] = get_random_image(handles.image_store, random_idx, patch_size);
    end
end
[random_patch, begin_random_x, begin_random_y] = get_random_patch(random_image, patch_size);

% Avoid non-invertible patches by drawing another one from the image.
number_of_attempts = 10; % How many times to attempt inverting a patch.
attempt = 1;
invertible = det(random_patch);
while(invertible == 0)
    [random_patch, begin_random_x, begin_random_y] = get_random_patch(random_image, patch_size);
    invertible = det(random_patch);
    attempt = attempt + 1;

    if(attempt == number_of_attempts) % Try another image, repeat.
        random_idx = randi(file_count);
        [random_image, file_info] = get_random_image(handles.image_store, random_idx, patch_size);
        attempt = 1;
        disp("Image was not invertible, trying another...");        
    end
end
% random_patch = flip_image_rand(random_patch);

patch_to_vec = random_patch(:);

args = struct;
args.min_pixel = min(patch_to_vec);
args.max_pixel = max(patch_to_vec);
args.mean_intensity = mean(patch_to_vec);
args.patch = random_patch;

% Rescale the originalpatch using the full range.
random_patch = rescale_pixels(args);
% Invert the patch using the full range.
inverted_patch = rescale_invert_pixels(args);

 % Find the position of the inverted patch
[inverse_idx, normal_idx] = get_patch_indices();
if(inverse_idx == left_panel)
    axes(handles.axes_left_patch);
    imshow(inverted_patch);
    
    axes(handles.axes_right_patch);
    imshow(random_patch);       
else
    axes(handles.axes_right_patch);
    imshow(inverted_patch);  
    
    axes(handles.axes_left_patch);
    imshow(random_patch);    
end
handles.current_image = random_image;
handles.current_patch = random_patch;

handles_updated = handles;
hobject_updated = hObject;

% Store patch locations.
file_info.begin_random_x = begin_random_x;
file_info.begin_random_y = begin_random_y;

images_path = fullfile(handles.application_configs.images_dir);
file_info.Filename = extractAfter(file_info.Filename, images_path); % Save path independent of machine.
end
