global application_configs;

% Import all required source
set_paths;

application_configs = struct;
application_configs.images_dir = '/home/z/Desktop/dataset/images'; % path to THINGS % 
application_configs.minimum_dimensions = 400; % 400x400 for THINGS

% Path to data file - if this does not exist, we will write a new file.
application_configs.data_file_path = 'response_data/response_data_flip.mat';

application_configs.no_flip_path = 'response_data/response_data_noflip.mat';
application_configs.flip_path = 'response_data/response_data_flip.mat';

file_path = application_configs.data_file_path;

application_configs.data_exists = false;
if(isfile(file_path)) % saved cell field must be "data_matrix"
      last_save = load(file_path);
      application_configs.last_save = last_save;
      application_configs.data_exists = true;
end

application_configs.auto_save_responses = false; % Automatically save after each trial.
% include 400x400 for human test.
application_configs.patch_range = [8, 16, 32, 64, 128, 256, 400];
application_configs.patch_range_count = length(application_configs.patch_range);
application_configs.max_patch_size = application_configs.patch_range(application_configs.patch_range_count);

application_configs.multiple_patches_per_img = false; % Select a new image per trial. If true use a single image per patch page range.
application_configs.patches_per_image = 1; % Number of random patches to extract per image. You can only increase this if multiple patches per img.

if(~(application_configs.multiple_patches_per_img) && application_configs.patches_per_image > 1)
    error("You must set 'multiple_patches_per_img' to true to enable this feature. Otherwise set 'patches_per_image' to 1.");
end

image_store = get_image_store(application_configs.images_dir);
application_configs.image_file_count = length(image_store.Files);

application_configs.image_store = image_store;

net = inceptionv3;
application_configs.pretrained_net = net;
