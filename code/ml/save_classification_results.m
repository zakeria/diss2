% A script to generate classification results in [stimuli#number correct] format.
function save_classification_results()
global application_configs;
% 8 - 256
patch_range = application_configs.patch_range;
% training_sizes = [500, 2000, 5000, 10000, 15000, 20000, 30000, 35000];
training_sizes = 2.^[8:14]; 

classification_size = 1024;

for i = 1:length(training_sizes)
    training_size = training_sizes(i);
    no_flip3 = [];
 for j = 1:length(patch_range)
    patch_size = patch_range(j);
 
     %  test data + svm dir denote one random split.
    image_store = get_image_store("test_data/" + num2str(patch_size));
    svm_file = load("svms/with_flipping/3/svm_classifier_" + num2str(training_size) + "_" + num2str(patch_size) + ...
      ".mat");
    svm_ = svm_file.svm_classifier;

    [predictions, accuracy] = classify_patches(image_store, svm_);
    disp("patch size: " + num2str(patch_size) + " num training data " + num2str(training_size));
    disp("accuracy: " + num2str(accuracy));
    
    data_ = [accuracy*classification_size classification_size];
    no_flip3 = [no_flip3; data_];
  end
  disp(no_flip3);
  save("svm_data/flip_exp/" + num2str(training_size) + "/no_flip3.mat", 'no_flip3');
end
end
