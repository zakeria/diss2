% Split dataset into a [training:ttest] split.
function create_data_split(training_size)
global application_configs;
image_store = application_configs.image_store;
[training_set, test_set] = splitEachLabel(image_store, training_size, 'randomized');
save('training_set.mat', 'training_set');
save('test_set.mat', 'test_set');
end
