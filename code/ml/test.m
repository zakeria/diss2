% No color allowed? - all the pretrained networks require RGB data.
% Read a patch size e.g. 128x128 resize by bilinear (no padding) to [299 299], get 
% output from the pooling layer, feed to svm.


% Get pooling layer output? (activaiton(...))

% we want datasets: ful stretched patches, and full stretched inverted patches. - we need to label these? for training.
function [svm_classifier] = train_classifier(num_trials)
global application_configs;

image_store = application_configs.image_store;

net = application_configs.pretrained_net;

% 80:30 split [training: test] data.
% extract patches from the training datam and feed inverted and normal to neural net - randomly selected
% only need the inverted and non inverted to be classified by the svm.
% strip object labels and leave inverted/normal when training svm.
% make sure to save the image data_store strings containing the split paths.
save_location = "image_data/";

[training_set, test_set] = splitEachLabel(image_store, 0.8,'randomized');
disp("Saving training and test datasets");
% add to config
num_of_patches_per_size = num_trials; % i.e. number of trials.
input_size = net.Layers(1).InputSize; 
input_row = input_size(1);
input_col = input_size(2);

patch_range = application_configs.patch_range; % will need a range of patch size training data.

training_file_count = length(training_set.Files);
training_labels = categorical({'normal', 'inverted'})';

num_training_images = training_file_count; 
num_patches = length(patch_range);

disp("Constructing patch data..");
data_count = 1; % also in the image_store.
for i = 1:num_patches 
  patch_size = patch_range(i);
  for j = 1:num_of_patches_per_size;
    [random_patch, file_info] = get_random_patch2(training_set, training_file_count, patch_size);

    patch_to_vec = random_patch(:);

    args = struct;
    args.min_pixel = min(patch_to_vec);
    args.max_pixel = max(patch_to_vec);
    args.mean_intensity = mean(patch_to_vec);
    args.patch = random_patch;

    % Rescale the originalpatch using the full range.
    random_patch = rescale_pixels(args);
    % Invert the patch using the full range.
    inverted_patch = rescale_invert_pixels(args);

    random_patch = imresize(random_patch, [input_row, input_col], 'bilinear');
    inverted_patch = imresize(inverted_patch, [input_row, input_col], 'bilinear');
    random_patch_rgb = cat(3, random_patch, random_patch, random_patch);
    inverted_patch_rgb = cat(3, inverted_patch, inverted_patch, inverted_patch);
 
    file_extension = file_info.Filename(end-3:end); % expecting only jpg or png. or 3 char extension name. 
    file_string_normal = save_location + "normal/" + string(file_info.Label) + "_" ...
      + num2str(patch_size) + file_extension;
    file_string_inverted= save_location + "inverted/" + string(file_info.Label) + "_" ...
      + num2str(patch_size) + file_extension;

    imwrite(random_patch_rgb, file_string_normal);
    imwrite(inverted_patch_rgb, file_string_inverted);

    data_count = data_count + 2;
end
end
% We use an image_store for memory efficiency e.g. vs raw 4D image data.
disp("Finished constructing training patch data.")
disp("Constructing test data store");

patch_training_data = get_image_store(save_location);


% Save image stores.
% - training_set - contains training images split
% - patch_training_data - contains image patches randomly selecte from the training set.
% - test_set - contains test images split.
save('training_set.mat', 'training_set');
save('patch_training_data.mat', 'patch_training_data');
save('test_set.mat', 'test_set');

% still need to construct test patches from test data set. to give prediction rate.
% only two labels inverted and non inverted for svm.
% save the image data store containing the paths to the training data and test data
disp("Obtaining training features from the network...");
layer = 'avg_pool';
training_features = activations(net, patch_training_data, layer,'OutputAs','rows');

disp("Fitting training data to the SVM...");
training_labels = patch_training_data.Labels;

svm_classifier = fitcecoc(training_features, training_labels);
% when classifying with the svm we will resize the patches once again from the test data set.
% from there we evaluate the patch size accuracy.
end
