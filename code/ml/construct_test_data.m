function construct_test_data(application_configs, number_of_trials, fixed_patch_size)
number_of_trials = number_of_trials / 2;
  disp("Constructing patch data..");
  data_count = 1; % also in the image_store.
  
  save_location_test = "test_data/";
  if(fixed_patch_size ~=0)
    save_location_test = fullfile(save_location_test, num2str(fixed_patch_size) + "/"); 
  end
    
  net = application_configs.pretrained_net;

  input_size = net.Layers(1).InputSize; 
  input_row = input_size(1);
  input_col = input_size(2);

  test_set_mat = load('test_set.mat');
  image_store = test_set_mat.test_set;
  
  test_file_count = length(image_store.Files);
  
  patch_range = application_configs.patch_range; % will need a range of patch size training data.

if(ismember(fixed_patch_size, patch_range))
   num_patches = 1;
 elseif(fixed_patch_size == 0)
   num_patches = length(patch_range);
 else
   error('Not a valid patch size');
end


parfor i = 1:num_patches
      if(ismember(fixed_patch_size, application_configs.patch_range))
          patch_size =  fixed_patch_size;
        else
          patch_size = patch_range(i);
          save_location_ = fullfile(save_location_test, num2str(patch_size) + "/"); 
        end
    for j = 1 : number_of_trials;
      [random_patch, file_info] = get_random_patch2(image_store, test_file_count, patch_size);
random_patch = flip_image_rand(random_patch);
      patch_to_vec = random_patch(:);

      args = struct;
      args.min_pixel = min(patch_to_vec);
      args.max_pixel = max(patch_to_vec);
      args.mean_intensity = mean(patch_to_vec);
      args.patch = random_patch;

      % Rescale the originalpatch using the full range.
      random_patch = rescale_pixels(args);
      % Invert the patch using the full range.
      inverted_patch = rescale_invert_pixels(args);

      random_patch = imresize(random_patch, [input_row, input_col], 'bilinear');
      inverted_patch = imresize(inverted_patch, [input_row, input_col], 'bilinear');
      random_patch_rgb = cat(3, random_patch, random_patch, random_patch);
      inverted_patch_rgb = cat(3, inverted_patch, inverted_patch, inverted_patch);
   
      file_extension = file_info.Filename(end-3:end); % expecting only jpg or png. or 3 char extension name. 
      file_string_normal = save_location_+ "normal/" + string(file_info.Label) + "_" ...
        + num2str(patch_size) + file_extension;
      file_string_inverted= save_location_+ "inverted/" + string(file_info.Label) + "_" ...
        + num2str(patch_size) + file_extension;

       imwrite_no_override(file_string_normal, random_patch_rgb, file_extension);
       imwrite_no_override(file_string_inverted, inverted_patch_rgb, file_extension);

      data_count = data_count + 2;
  end
  end
end
