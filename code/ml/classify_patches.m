function [predictions, accuracy] = classify_patches(image_store, svm_)
global application_configs;
net = application_configs.pretrained_net;
layer = 'avg_pool';

tic;
test_features = activations(net, image_store, layer,'OutputAs','rows', 'MiniBatchSize', 128, ...
  'ExecutionEnvironment', 'GPU');
toc;
predictions = predict(svm_, test_features);
expected_labels = image_store.Labels;

classifications = (predictions == expected_labels);
accuracy = mean(classifications);
end
