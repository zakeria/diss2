% Train svm per patch size.
% Training data is constructed prior using construct_training_data(num_patches, patch_size
function train_classifiers(application_configs, patch_size_)
patch_range = application_configs.patch_range;

if(~ismember(patch_size_, patch_range) && patch_size_ ~= 0)
  error("Invalid patch size " + num2str(patch_size_));
end

if(patch_size_ ~= 0)
  loop_range = 1;
else
  loop_range = length(patch_range);
end

net = application_configs.pretrained_net; 
disp("Obtaining training features from the network...");
layer = 'avg_pool';
tic;
image_store_8 = get_image_store("training_data/8");
image_store_16 = get_image_store("training_data/16");
image_store_32 = get_image_store("training_data/32");
image_store_64 = get_image_store("training_data/64");
image_store_128 = get_image_store("training_data/128");
image_store_256 = get_image_store("training_data/256");

features_8 = activations(net, image_store_8, layer,'OutputAs','rows', 'MiniBatchSize', 128, 'ExecutionEnvironment', 'GPU');
features_16 = activations(net, image_store_16, layer,'OutputAs','rows', 'MiniBatchSize', 128, 'ExecutionEnvironment', 'GPU');
features_32 = activations(net, image_store_32, layer,'OutputAs','rows', 'MiniBatchSize', 128, 'ExecutionEnvironment', 'GPU');
features_64 = activations(net, image_store_64, layer,'OutputAs','rows', 'MiniBatchSize', 128, 'ExecutionEnvironment', 'GPU');
features_128 = activations(net, image_store_128, layer,'OutputAs','rows', 'MiniBatchSize', 128, 'ExecutionEnvironment', 'GPU');
features_256 = activations(net, image_store_256, layer,'OutputAs','rows', 'MiniBatchSize', 128, 'ExecutionEnvironment', 'GPU');

feature_store{1} = features_8;
feature_store{2} = features_16;
feature_store{3} = features_32;
feature_store{4} = features_64;
feature_store{5} = features_128;
feature_store{6} = features_256;

clear image_store_8;
clear image_store_16;
clear image_store_32;
clear image_store_64;
clear image_store_128;
clear image_store_256;
toc;

tic;
parfor i = 1:loop_range
  if(loop_range ~= 1)
    patch_size = patch_range(i);
  else
    patch_size = patch_size_;
  end
  image_store = get_image_store("training_data/" + num2str(patch_size));
  training_count = length(image_store.Files);

  disp(" ");
  disp("training svm " + num2str(patch_size));
  disp("Training size is: " + num2str(training_count)); 
  
  features_ = feature_store{i}; 
  svm_classifier = train_classifier(application_configs, image_store, features_, patch_size);
  disp(" ");
 % save("svms/svm_classifier" + "_" + num2str(training_count) + "_" + num2str(patch_size) + ".mat",...
    %   'svm_classifier'); 
    par_save("svms/svm_classifier" + "_" + num2str(training_count) + "_" + num2str(patch_size) + ".mat", svm_classifier);
end
training_time = toc;
disp("Total training time was: " + num2str(training_time));
end 
