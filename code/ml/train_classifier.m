function [svm_classifier] = train_classifier(application_configs, patch_training_data, training_features, patch_size)
net = application_configs.pretrained_net; 

% n_images = length(patch_training_data.Files);
% patch_range = length(application_configs.patch_range);
% n_trials = (n_images / 2) / patch_range;
% disp("Loading training data from " + num2str(n_trials) + " trials.");
% disp("Total number of patch data is: " + num2str(n_images) + "." );


training_labels = patch_training_data.Labels;

disp("Fitting training data to the SVM..." + num2str(patch_size));

tic;
svm_classifier = fitcecoc(training_features, training_labels);
total_training_time = toc;
disp("Training time was " + num2str(total_training_time) + " seconds.");
end
