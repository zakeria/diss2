function train_automate()
global application_configs;
sizes = (2.^[8:14])/2;

construct_training_data(2*sizes(1),0);
train_classifiers(application_configs, 0);

for i = 2:length(sizes)
  construct_training_data(sizes(i),0);
  train_classifiers(application_configs, 0);
  end
end
