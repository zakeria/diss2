function classify_all()
global application_configs;

patch_range = application_configs.patch_range;
training_sizes = [500, 2000, 5000, 10000, 15000, 20000, 30000, 35000];

for i = 1:length(patch_range)
  patch_size = patch_range(i);

  image_store = get_image_store("test_data/" + num2str(patch_size));
  for j = 1:length(training_sizes)
    training_size = training_sizes(j);
    svm_file = load("svms/with_flipping/1/svm_classifier_" + num2str(training_size) + "_" + num2str(patch_size) + ...
      ".mat");
    svm_ = svm_file.svm_classifier;

    [~, accuracy] = classify_patches(image_store, svm_);

    disp("patch size: " + num2str(patch_size) + " num training data " + num2str(training_size));
    disp("accuracy: " + num2str(accuracy));

    disp(" ");
  end
end
end
