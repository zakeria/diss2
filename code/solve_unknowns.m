function [ alpha, beta] = solve_unknowns(A,b)
x = A\b;
alpha = x(1);
beta = x(2);
end

