function result_patch = rescale_invert_pixels(args)
min_intensity = args.min_pixel;
max_intensity = args.max_pixel;

A = [max_intensity 1; min_intensity 1];
b = [0 1]';

[alpha, beta] = solve_unknowns(A,b); 

result_patch = alpha * args.patch + beta;
end 