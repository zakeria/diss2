function result_patch = rescale_invert_pixels_mean(args)
min_intensity = args.min_pixel;
max_intensity = args.max_pixel;
mean_intensity = args.mean_intensity;

A = [max_intensity 1; min_intensity 1];

if(mean_intensity-min_intensity >= mean_intensity-max_intensity)
    b = [0 0.5]';
elseif(mean_intensity-max_intensity > mean_intensity-min_intensity)
    b = [1 0.5]'; 
end  

[alpha, beta] = solve_unknowns(A,b);

result_patch = alpha * args.patch + beta; 
end