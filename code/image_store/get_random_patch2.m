% 
function [random_patch, file_info] = get_random_patch2(image_store, file_count, patch_size)
number_of_attempts = 10; % How many times to attempt inverting a patch.
attempt = 1;

% initial image.
random_idx = randi(file_count);
[random_image, file_info] = get_random_image(image_store, random_idx, patch_size);
random_patch = get_random_patch(random_image, patch_size);
invertible = det(random_patch);
while(invertible == 0)
    [random_patch, begin_random_x, begin_random_y] = get_random_patch(random_image, patch_size);
    invertible = det(random_patch);
    attempt = attempt + 1;

    if(attempt == number_of_attempts) % Try another image, repeat.
        random_idx = randi(file_count);
        [random_image, file_info] = get_random_image(image_store, random_idx, patch_size);
        attempt = 1;
        disp("Image was not invertible, trying another...");        
    end
end
end
