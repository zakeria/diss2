function imwrite_no_override(FileName, Data, fExt)
exist_count = 1;
file_name_new = FileName;
while(exist(file_name_new, 'file'));
    [fPath, fName, fExt] = fileparts(FileName);
    fName = fName + "_d" + num2str(exist_count);
    file_name_new = fullfile(fPath, fName + fExt);
    exist_count = exist_count + 1;
end
imwrite(Data, file_name_new);
