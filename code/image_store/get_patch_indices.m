% Index 1: left panel
% Index 2: Is right panel
function [inverted_idx, normal_idx] = get_patch_indices()

inverted_idx = randi(2);

if(inverted_idx == 1)
    normal_idx = 2;
else
    normal_idx = 1;
end
end
