% Extract a random patch from the image.
% Random location within the image, subtract patch size.
function [res_patch, begin_random_x, begin_random_y] = get_random_patch(I, patch_size)
[rows, cols] = size(I);

% rows or cols can only be >= patch_size.
% handle the dimensions cases for extracting a patch.
if(rows == patch_size && cols == patch_size)
    res_patch = I;
    begin_random_x = 1;
    begin_random_y = 1;
    return;
elseif(rows == patch_size && cols > patch_size)
    begin_random_x = 1; % e.g. randi(512-512) not possible.
    begin_random_y = randi(cols-patch_size);
elseif(rows > patch_size && cols == patch_size)
    begin_random_x = randi(rows-patch_size);
    begin_random_y = 1;
else
    begin_random_x = randi(rows-patch_size);
    begin_random_y = randi(cols-patch_size);
end
res_patch = I(begin_random_x:begin_random_x+patch_size-1, begin_random_y:begin_random_y+patch_size-1);
end

