% Load all images in given folder, and store them as a 'cache' of strings
% that point to the image files on disk.
% Find multiple file extensions if required.
function image_store = get_image_store(images_dir)
image_store = datastore(fullfile(images_dir),...
'IncludeSubfolders', true, 'LabelSource','foldernames','FileExtensions', '.jpg','Type', 'image');
end

