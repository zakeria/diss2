% Read and retrieve a random image from the store as a grayscale image.
function [image, file_info] = get_random_image(image_store, random_index, patch_size)

[image, file_info] = image_store.readimage(random_index);

file_count = length(image_store.Files);
[rows, cols, channels] = size(image);
 
assert(rows >= 400 && cols >= 400, "Bad dimensions");

while(patch_size > rows || patch_size > cols) 
    random_index = randi(file_count);    
    image = image_store.readimage(random_index);
    [rows, cols] = size(image);
end

image = imresize(image, [400, 400]);
image = im2double(image);

if(channels == 3)
    image = rgb2gray(image);
end
end