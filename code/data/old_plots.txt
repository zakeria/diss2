% plot asymptotes
% upper_bound1 = fminbnd(@(x)-fit_human_flip1.psiHandle(x),1,500);
% upper_bound2 = fminbnd(@(x)-fit_human_flip2.psiHandle(x),1,500);
% upper_bound3 = fminbnd(@(x)-fit_human_flip3.psiHandle(x),1,500);

% max1 =  fit_human_flip1.psiHandle(upper_bound1);
% max2 =  fit_human_flip2.psiHandle(upper_bound2);
% max3 =  fit_human_flip3.psiHandle(upper_bound3);

% maxes_ = [max1 max2 max3];
% min_h = min(maxes_);
% max_h = max(maxes_);

% hline(max_h, 'c--', '');
% hline(min_h, 'c--', '');

% threshold_first_exp = getThreshold(fit_first_exp_ml, 0.75);
% disp([threshold_human_flip threshold_first_exp]);
% disp([thresh1 thresh2 thresh3])
% figure;
% title({'Results human vs network - average' 'With top-bottom flipping'});
% options.lineColor = [1, 0, 0];
% [hline1,hdata1] = plotPsych(fit_human_flip, options);
% vline(threshold_human_flip, 'r', '');

% hold on
% options.lineColor = [0, 0, 0];
% [hline2,hdata2] = plotPsych(fit_first_exp_ml, options);
% vline(threshold_first_exp, 'b', '');

% legend([hline1, hline2], 'Fit human', 'Fit network');

% hline(0.75, 'black', '');
% --------------- Confidence intervals
% These Confidence intervals were found by the <xxx> bootstrap method implemented by psignifit, 
% based on <nnn> simulations from "Wichmann and Hill", 2001b. 
% fit_results = fit_human_no_flip.Fit;
% confidence_intervals = fit_results.conf_Intervals;

% plot_options = struct;
% plot_options.xLabel = 'Log2 patch size'; % xlabel text
% plot_options.labelSize = 12; % label font size
% plot_options.plotPE = true;
% plot_options.confP = .95;
% plot_options.CIthresh = false;
% plot_options.plotThresh = false;
% plot_options.plotAsymptote = false;
% plot_options_estimateType = 'MAP';
% plot_options.useGPU = 1;

% confidence_interval = fit_human_flip.conf_Intervals;
% confidence_interval
% figure,plotMarginal(fit_human_flip,1,plot_options);
% title({"Bootstrap confidence interval", "With top-bottom flipping"});