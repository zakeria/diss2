% psignifit Psychmetric toolbox from:  https://github.com/wichmann-lab/psignifit
% https://github.com/wichmann-lab/psignifit/wiki/Basic-Usage
% https://github.com/wichmann-lab/psignifit/wiki/Plot-Functions
function compare_results_no_flip()
global application_configs;

data_exists = application_configs.data_exists;
if(~data_exists)
    error("Data file: " + application_configs.data_file_path + " does not exist");
    return;
end
data_cell = load(application_configs.no_flip_path); 
[num_cells, ~] = size(data_cell);

patch_range = application_configs.patch_range;
max_patch = patch_range(end);

% form data matrix as matrix first - original form.
% Combines data from multiple sessions
raw_response_matrix = get_raw_data_matrix(application_configs, "no_flip");

% Reconstrut data matrix into plot format.
stimilus_range = length(patch_range);

plot_data = zeros(stimilus_range, 3); % psignifit format

row_count = size(raw_response_matrix, 1);
n_count = row_count;

[rows, cols] = size(raw_response_matrix);
row_idx = 1;
for i=1:cols
    stimilus_level = log2(patch_range(i));
    number_correct = nnz(raw_response_matrix(:,i));    
    
    plot_data(row_idx, 1) = stimilus_level;
    plot_data(row_idx, 2) = number_correct;
    plot_data(row_idx, 3) = n_count;
    
    row_idx = row_idx + 1;    
end
plot_data
options = struct;               % initialize options struct
options.sigmoidName = 'norm';   % choose a cumulative Gauss as the sigmoid  
options.expType     = '2AFC';   % choose 2-AFC as the experiment type  
                                % this sets the guessing rate to .5 (fixed) and  
                                % fits the rest of the parameters
options.threshPC       = .75;
options.confP          = .95;
options.labelSize = 10; % label font size
options.CIthresh = false;
options.plotThresh = false;
options.prior = false;
options.plotAsymptote = false;
options.useGPU = 1;
options.xLabel = "Log patch size"

svm_path = "svm_data/no_flip_exp/" + num2str(20000);

first_exp = load(svm_path + "/no_flip1.mat");
second_exp = load(svm_path + "/no_flip2.mat");
third_exp = load(svm_path + "/no_flip3.mat");
avg_exp = (first_exp.no_flip1 + second_exp.no_flip2 + third_exp.no_flip3)./3;
avg_exp

patch_range_ml = log2(patch_range(1:end-1));

fit_first_exp_ml = psignifit([patch_range_ml' avg_exp], options);
fit_human_no_flip = psignifit(plot_data, options);

threshold_human_no_flip = getThreshold(fit_human_no_flip, 0.75);
threshold_first_exp = getThreshold(fit_first_exp_ml, 0.75);
disp([threshold_human_no_flip threshold_first_exp]);

figure;
% avg performance accross splits, TODO also show separate figures of to analyse training splits directly
% for both experiments - copy plotting code to exp_withflip
title({'Results human vs network - training split 3' 'No top-bottom flipping'});
options.lineColor = [1, 0, 0];
[hline1,hdata1] = plotPsych(fit_human_no_flip, options);
vline(threshold_human_no_flip, 'r', '');

hold on
options.lineColor = [0, 0, 1];
[hline2,hdata2] = plotPsych(fit_first_exp_ml, options);
vline(threshold_first_exp, 'b', '');

legend([hline1, hline2], 'Fit human', 'Fit network');
hline(.75, 'black', '');



% --------------- Confidence intervals
% These Confidence intervals were found by the <xxx> bootstrap method implemented by psignifit, 
% based on <nnn> simulations from "Wichmann and Hill", 2001b. 
% fit_results = fit_human_no_flip.Fit;
% confidence_intervals = fit_results.conf_Intervals;

plot_options = struct;
plot_options.xLabel = 'Log patch size'; % xlabel text
plot_options.labelSize = 12; % label font size
plot_options.plotPE = true;
plot_options.confP = .95;
plot_options.CIthresh = false;
plot_options.plotThresh = false;
plot_options.plotAsymptote = false;
plot_options.prior = false; 
plot_options_estimateType = 'MAP';
plot_options.useGPU = 1;

confidence_interval = fit_human_no_flip.conf_Intervals;
confidence_interval
figure,plotMarginal(fit_human_no_flip,1,plot_options);
title({"Bootstrap confidence interval", "No top-bottom flipping"});
end
