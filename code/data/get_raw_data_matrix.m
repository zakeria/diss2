function raw_matrix = get_raw_data_matrix(application_configs, exp_type)

data_exists = application_configs.data_exists;
if(~data_exists)
    error("Data file: " + application_configs.data_file_path + " does not exist");
    return;
end
if(strcmp(exp_type, "flip"))
  data_cell = load(application_configs.flip_path); 
elseif(strcmp(exp_type, "no_flip"))
  data_cell = load(application_configs.no_flip_path); 
else
  error("invalid experiment type");
end

data_cell = data_cell.data_matrix;
raw_matrix = zeros(1); % dynamic
[rows, cols] = size(data_cell);
for i=1:rows
    for j = 1:cols
        response_ = data_cell{i, j}.response;
        raw_matrix(i,j) = response_;
    end
end
end
