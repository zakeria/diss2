function plot_first_experiment()
global application_configs;

patch_range = application_configs.patch_range(1:end-1);
log_patch_range = log2(patch_range)';

% If i plot median graph, will that give median of thresh vals?
% First random split results
options = struct;               % initialize options struct
options.sigmoidName = 'norm';   % choose a cumulative Gauss as the sigmoid  
options.expType     = '2AFC';   % choose 2-AFC as the experiment type  
                                % this sets the guessing rate to .5 (fixed) and  
                                % fits the rest of the parameters
                                
% options.expType        = 'YesNo';
% options.expType = 'equalAsymptote';
options.xLabel = 'Log_2 Patch Size'; % xlabel text
options.yLabel = 'Classification Accuracy'; 

options.labelSize = 12; % label font size
options.plotThresh = true;
options.plotData = false;
options.threshPC = 0.75;

training_sizes = [500 2000 5000 10000 15000 20000];

curve = zeros(1, length(training_sizes)); 
for i = 1:length(training_sizes) 
 svm_path = "svm_data/no_flip_exp/" + num2str(training_sizes(i));
 first_exp = load(svm_path + "/no_flip1.mat");
 second_exp =  load(svm_path + "/no_flip2.mat");
 third_exp = load(svm_path + "/no_flip3.mat");

 fit_first_exp = psignifit([log_patch_range first_exp.no_flip1], options);
 fit_second_exp = psignifit([log_patch_range second_exp.no_flip2], options);
 fit_third_exp = psignifit([log_patch_range third_exp.no_flip3], options);

% disp("training size  " + num2str(training_sizes(i)) + ":");
% disp(" ");
% disp("Experiment without flipping");
% split1_2= [2.^log_patch_range first_exp2.no_flip1];
% split2_2 = [2.^log_patch_range second_exp2.no_flip2];
% split3_2 = [2.^log_patch_range third_exp2.no_flip3];

% avg_split2 = (split1_2 + split2_2 + split3_2)./3;

% disp(avg_split2);
% disp("experiment with flipping");
% split1_1 = [2.^log_patch_range first_exp.no_flip1];
% split2_1 = [2.^log_patch_range second_exp.no_flip2];
% split3_1 = [2.^log_patch_range third_exp.no_flip3];

% avg_split1 = (split1_1 + split2_1 + split3_1)./3;
% disp(avg_split1);


% figure;
% title("Psychometric fn for training size: " + num2str(training_sizes(i)) + "- random split 2");
% plotPsych(fit_second_exp);
% xlabel("Patch size");
% disp([log_patch_range third_exp.no_flip3]);
% thresh = getThreshold(fit_second_exp, 0.75);

   % determine from fit results if the 75% is reached, plot thresh values that reach this.
 % include splits which reach this? and exclude the others out 3 who dont?
  upper_bound1 = fminbnd(@(x)-fit_first_exp.psiHandle(x),1,500);
  upper_bound2 = fminbnd(@(x)-fit_second_exp.psiHandle(x),1,500);
  upper_bound3 = fminbnd(@(x)-fit_third_exp.psiHandle(x),1,500);

  max_first_exp =  fit_first_exp.psiHandle(upper_bound1);
  max_second_exp = fit_second_exp.psiHandle(upper_bound2);
  max_third_exp =  fit_third_exp.psiHandle(upper_bound3);

  % Get the maximum value for the y-axis from the psychometric functions
  % This will tell us if there is an estimate available up to 2^9 patch size.
  plot_maxes = [max_first_exp max_second_exp max_third_exp];
  indices_ = find(plot_maxes >= .75);

  thresh1 = 0;
  thresh2 = 0;
  thresh3 = 0;
  if(ismember(1, indices_))
    thresh1 = getThreshold(fit_first_exp, 0.75);
  end
  if(ismember(2, indices_))
   thresh2 = getThreshold(fit_second_exp, 0.75);  
  end
  if(ismember(3, indices_))
   thresh3 = getThreshold(fit_third_exp, 0.75); 
  end
  % Place thresholds in orderj
  thresh_ = [thresh1 thresh2 thresh3];
  if(length(indices_) == 0)
    curve(i) = 0; 
    continue;
  end;
  % Get the mean threshold value
  mean_thresh = mean(thresh_(indices_));
  x_data = log2(training_sizes(i));
  y_data = mean_thresh;
  if(length(indices_ > 1))
    min_ = min(thresh_(indices_));
    max_ = max(thresh_(indices_));

    dist_down = abs(y_data - min_);
    dist_up = abs(y_data - max_);

    errorbar(x_data, y_data, dist_down, dist_up, 'ro');
    hold on;
% plot(x_data, min_, 'g*');
% plot(x_data, max_, 'b*');
    else
    errorbar(x_data, y_data, 0, 0, 'ro');
  end
    curve(i) = mean_thresh;
end
title({"Network training size versus 75% log_2 patch size threshold", "Without top-bottom flipping"});
xlabel("Log_2 training size");
ylabel("Log_2 patch size for 75% accuracy");
xlim([8 14.5]);
% plot(log2(training_sizes), curve);
end
