% psignifit Psychmetric toolbox from:  https://github.com/wichmann-lab/psignifit
% https://github.com/wichmann-lab/psignifit/wiki/Basic-Usage
% https://github.com/wichmann-lab/psignifit/wiki/Plot-Functions
function compare_results_no_flip()
global application_configs;

data_exists = application_configs.data_exists;
if(~data_exists)
    error("Data file: " + application_configs.data_file_path + " does not exist");
    return;
end
data_cell = load(application_configs.no_flip_path); 
[num_cells, ~] = size(data_cell);

patch_range = application_configs.patch_range;
max_patch = patch_range(end);

% form data matrix as matrix first - original form.
% Combines data from multiple sessions
raw_response_matrix = get_raw_data_matrix(application_configs, "no_flip");

% Reconstrut data matrix into plot format.
stimilus_range = length(patch_range);

plot_data1 = zeros(stimilus_range, 3); % psignifit format
plot_data2 = zeros(stimilus_range, 3); % psignifit format
plot_data3 = zeros(stimilus_range, 3); % psignifit format

plot_data_aggregated = zeros(stimilus_range, 3); % psignifit format


row_count = size(raw_response_matrix, 1);
n_count = row_count;

[rows, cols] = size(raw_response_matrix);
row_idx = 1;
for i=1:cols
    stimilus_level = log2(patch_range(i));

    number_correct1 = nnz(raw_response_matrix(1:100,i));    
    plot_data1(row_idx, 1) = stimilus_level;
    plot_data1(row_idx, 2) = number_correct1;
    plot_data1(row_idx, 3) = 100; % 1 participant
    
    number_correct2 = nnz(raw_response_matrix(101:200, i));    
    plot_data2(row_idx, 1) = stimilus_level;
    plot_data2(row_idx, 2) = number_correct2;
    plot_data2(row_idx, 3) = 100; % 1 participant
 
    number_correct3 = nnz(raw_response_matrix(201:300, i));    
    plot_data3(row_idx, 1) = stimilus_level;
    plot_data3(row_idx, 2) = number_correct3;
    plot_data3(row_idx, 3) = 100; % 1 participant
 
    number_correct_aggregated = nnz(raw_response_matrix(:, i));    
    plot_data_aggregated(row_idx, 1) = stimilus_level;
    plot_data_aggregated(row_idx, 2) = number_correct_aggregated;
    plot_data_aggregated(row_idx, 3) = 300; 

    row_idx = row_idx + 1;    
end

options = struct;               % initialize options struct
options.sigmoidName = 'norm';   % choose a cumulative Gauss as the sigmoid  
options.expType     = '2AFC';   % choose 2-AFC as the experiment type  
                                % this sets the guessing rate to .5 (fixed) and  
                                % fits the rest of the parameters
options.threshPC       = 0.75;
options.plotAsymptote = false;
options.confP          = .95;
options.labelSize = 10; % label font size
options.plotThresh = false;
options.CIthresh = false;
options.xLabel = "Log_2 patch size"
svm_path = "svm_data/no_flip_exp/" + num2str(20000);

first_exp = load(svm_path + "/no_flip1.mat");
second_exp = load(svm_path + "/no_flip2.mat");
third_exp = load(svm_path + "/no_flip3.mat");
avg_exp = (first_exp.no_flip1 + second_exp.no_flip2 + third_exp.no_flip3)./3;
avg_exp

patch_range_ml = log2(patch_range(1:end-1));

% Mean across training splits
fit_first_exp_ml = psignifit([patch_range_ml' avg_exp], options);
thresh_ml = getThreshold(fit_first_exp_ml, 0.75);

fit_human_flip1 = psignifit(plot_data1, options);
fit_human_flip2 = psignifit(plot_data2, options);
fit_human_flip3 = psignifit(plot_data3, options);
fit_human_flip_aggregated = psignifit(plot_data_aggregated, options);

thresh1 = getThreshold(fit_human_flip1, 0.75);
thresh2 = getThreshold(fit_human_flip2, 0.75);
thresh3 = getThreshold(fit_human_flip3, 0.75);
thresh_aggregated = getThreshold(fit_human_flip_aggregated, 0.75);



figure;
title({'Responses from human participants' 'Without top-bottom flipping'});

options.lineColor = [1, 0, 0];
options.dataColor = [1, 0, 0];
[hline1,hdata1] = plotPsych(fit_human_flip1, options);
xlim([1 10]);
ylim([0.45 1]);

hold on;

options.lineColor = [0, 1, 0];
options.dataColor = [0, 1, 0];
[hline2,hdata2] = plotPsych(fit_human_flip2, options);
xlim([1 10]);
ylim([0.45 1]);

hold on;

options.lineColor = [0, 0, 1];
options.dataColor = [0, 0, 1];
[hline3, hdata3] = plotPsych(fit_human_flip3, options);
xlim([1 10]);
ylim([0.45 1]);


legend([hline1, hline2, hline3], 'First participant', 'Second participant', 'Third participant');
vline(thresh1, 'r--', '');
vline(thresh2, 'g--', '');
vline(thresh3, 'b--', '');
hline(0.75, 'black--', '');

% -----------------

figure;

title({'Aggregated responses from human participants' 'Without top-bottom flipping'});

options.lineColor = [0, 0, 0];
options.dataColor = [0, 0, 0];
[hline_a,hdata_a] = plotPsych(fit_human_flip_aggregated, options);
xlim([1 10]);
ylim([0.45 1]);

hold on;

threshes_ = [thresh1 thresh2 thresh3];
min_t = min(threshes_);
max_t = max(threshes_);
diff_t = abs(min_t - max_t); 
x1 =  [min_t max_t max_t min_t];
y1 =  [0     0     1     1];
p = patch(x1, y1, [.8 .8 .8]);
p.EdgeColor = [.8 .8 .8];
p.FaceAlpha = 0.6;

options.plotAsymptote = false;
options.plotData = false;
options.lineColor = [0, 0, 1];

vline(thresh_aggregated, 'black--', '');
hline(0.75, 'black--', '');

% ---- 2nd fig
figure;
title({'Aggregated responses from human participants' 'Without top-bottom flipping'});
options.plotAsymptote = false;
options.plotData = true;
options.lineColor = [0, 0, 0];
options.dataColor = [0, 0, 0];
[hline_a,hdata_a] = plotPsych(fit_human_flip_aggregated, options);
xlim([1 10]);
ylim([0.45 1]);

vline(thresh_aggregated, 'black--', '');
hline(0.75, 'black--', '');

figure;
title({'Responses from network' 'Without top-bottom flipping'});
options.plotData = true;
options.lineColor = [0, 0, 0];
options.dataColor = [0, 0, 0];
[hline_a,hdata_a] = plotPsych(fit_first_exp_ml, options);

vline(thresh_ml, 'black--', '');
hline(0.75, 'black--', '');
xlim([1 10]);
% ylim([0.45 1]);
end
