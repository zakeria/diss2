% Extract the response information from the save file, at the given trial
% number and patch size.
function response_data = get_response_data(trial_number, patch_size)

global application_configs;

data_exists = application_configs.data_exists;
if(~data_exists)
    error("Data file: " + application_configs.data_file_path + " does not exist");
    return;
end
data_cell = application_configs.last_save.data_matrix;

[rows, ~] = size(data_cell);

if(rows < trial_number)
    error("Trial number " + num2str(trial_number) + " does not exist in the data file.");
    return;
end

patch_range = application_configs.patch_range;

patch_size_exists = ismember(patch_size, patch_range);
if(~patch_size_exists)
    error("Invalid patch size: " + num2str(patch_size));
    return;
end
 
patch_idx = find(patch_range == patch_size);

response_ = data_cell{trial_number, patch_idx}.response;
response_data = data_cell{trial_number, patch_idx}.file_info;

begin_random_x = response_data.begin_random_x;
begin_random_y = response_data.begin_random_y;

path_to_image = fullfile(application_configs.images_dir, '/', response_data.Filename);
response_image = imread(path_to_image);
response_image = imresize(response_image, [400, 400]);
response_image = im2double(response_image);

[~, ~, channels] = size(response_image);
if(channels == 3)
    response_image = rgb2gray(response_image);
end

image_patch = response_image(begin_random_x:begin_random_x+patch_size-1, begin_random_y:begin_random_y+patch_size-1); 

patch_to_vec = image_patch(:);
args = struct;
args.min_pixel = min(patch_to_vec);
args.max_pixel = max(patch_to_vec);
args.mean_intensity = mean(patch_to_vec);
args.patch = image_patch;

% Rescale the originalpatch using the full range.
original_patch = rescale_pixels(args);
% Invert the patch using the full range.
inverted_patch = rescale_invert_pixels(args);
 
% Append additional data.
response_data.response_ = response_;
response_data.response_image = response_image;
response_data.original_patch = original_patch;
response_data.inverted_patch = inverted_patch;
response_data.full_path_image = path_to_image;
end