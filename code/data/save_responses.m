% Save the response data for the current session.
% Each session will be saved as a cell, which contains information such as
% the response (0 = incorrect, 1 = correct), the patch size denoted by the
% collumn index, the filename for the image and the patch offsets.
function save_status = save_responses(normal_idx, configs, data_matrix, current_patch_idx, spam_save_block)
file_path = configs.data_file_path;

cols_range = configs.patch_range_count;

[rows, cols] = size(data_matrix);

save_status = 0;

if(spam_save_block == 1 || spam_save_block == 2)
    disp("You cannot resave");
    return;
end

if(cols_range ~= cols)
    disp("You must finish the current trial before saving.");
    return;
end

% Return codes - the three cases.

% TODO access cell file and do properly.
if(cols == cols_range && rows > 1) % At least one row in data matrix.
    if(current_patch_idx > 1 && current_patch_idx <= 7) % Current trial in progress, will be saved next time when completed.
        data_matrix = data_matrix(1:rows-1, 1:cols);    % Slice off incomplete trial. 
        save_status = 2; % Return code for update_data_matrix().
    elseif(current_patch_idx == 1) % No incomplete trials, new row for the data_matrix is only created after patch_idx 1 response.
        save_status = 1;
    end
else
    % Only one row of data.
    save_status = 1; % Normal save, Clear the data matrix for next save.
end

 % TODO update file_path to new save destination is dimensions are different.
if(isfile(file_path)) % matrix field must be "data_matrix"
      last_save = load(file_path);
      data_matrix = [last_save.data_matrix; data_matrix]; % to stack within the same cell use "[ ]" operands.
end
save(file_path, 'data_matrix');
end